# CTF Write-Ups

---

## By Competition

### 2020
* [RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)
* [Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)
* [UTCTF 2020](https://gitlab.com/b0uldrr/utctf-2020)
* [Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)
* [Tamu CTF 2020](https://gitlab.com/b0uldrr/tamuctf-2020)
* [Midnight Sun CTF Quals 2020](https://gitlab.com/b0uldrr/midnight-sun-ctf-quals-2020)
* [AUCTF 2020](https://gitlab.com/b0uldrr/auctf-2020)
* [DawgCTF 2020](https://gitlab.com/b0uldrr/dawgctf-2020)
* [WPICTF 2020](https://gitlab.com/b0uldrr/wpictf-2020)
* [Houseplant CTF 2020](https://gitlab.com/b0uldrr/houseplant-ctf-2020)
* [HSCTF 2020](https://gitlab.com/b0uldrr/hsctf-2020/)
* [DownUnder CTF 2020](https://gitlab.com/b0uldrr/downunder-ctf-2020)
* [Syskron CTF 2020](https://gitlab.com/b0uldrr/syskronctf-2020)
* [Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)

### 2021
* [justCTF 2020](https://gitlab.com/b0uldrr/justctf-2020)
* [NahamCon CTF 2021](https://gitlab.com/b0uldrr/nahamcon-ctf-2021)
* [HSCTF 2021](https://gitlab.com/b0uldrr/hsctf-2021)
* [Redpwn CTF 2021](https://gitlab.com/b0uldrr/redpwn-ctf-2021)

---

## By Category

### Pwn
| Like | Name | Key Concepts | Writeup |
|------|------|--------------|---------|
|✔|bof|bof, rop, 64 bit, parameters|[UTCTF 2020](https://gitlab.com/b0uldrr/utctf-2020)|
|✔|canary|bof, format string, canary|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
| |no canary|bof|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
| |admpanel|staic anlysis, command concatenation|[Midnight Sun CTF Quals 2020](https://gitlab.com/b0uldrr/midnight-sun-ctf-quals-2020)|
| |easy as pie|acl, permissions|[AUCTF 2020](https://gitlab.com/b0uldrr/auctf-2020)|
| |turkey|bof|[AUCTF 2020](https://gitlab.com/b0uldrr/auctf-2020)|
|✔|bof to the top|bof, 32 bit, parameters|[DawgCTF 2020](https://gitlab.com/b0uldrr/dawgctf-2020)|
| |on lockdown|bof|[DawgCTF 2020](https://gitlab.com/b0uldrr/dawgctf-2020)|
| |tom nook the capitalist racoon| |[DawgCTF 2020](https://gitlab.com/b0uldrr/dawgctf-2020)|
| |dorsia1|bof|[WPICTF 2020](https://gitlab.com/b0uldrr/wpictf-2020)|
| |boredom|bof, 64 bit|[HSCTF 2020](https://gitlab.com/b0uldrr/hsctf-2020/)|
| |shell this!|bof, 64 bit|[DownUnder CTF 2020](https://gitlab.com/b0uldrr/downunder-ctf-2020)|
|✔|dROPit|bof, rop, ret2libc, 64 bit|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
| |greeter|bof|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
| |ret2basic|bof, 64 bit|[NahamCon CTF 2021](https://gitlab.com/b0uldrr/nahamcon-ctf-2021)|
| |stonks|bof, 64 bit, Ghidra|[HSCTF 2021](https://gitlab.com/b0uldrr/hsctf-2021)|
| |beginner generic pwn number 0|bof, 64-bit|[Redpwn CTF 2021](https://gitlab.com/b0uldrr/redpwn-ctf-2021)|
| |printf please|printf, 64-bit|[Redpwn CTF 2021](https://gitlab.com/b0uldrr/redpwn-ctf-2021)|
| |ret2generic flag reader|bof, 64-bit|[Redpwn CTF 2021](https://gitlab.com/b0uldrr/redpwn-ctf-2021)|
| |ret2the unknown|bof, ret2libc, ROP, 64-bit|[Redpwn CTF 2021](https://gitlab.com/b0uldrr/redpwn-ctf-2021)|
 

### Rev
| Like | Name | Key Concepts | Writeup |
|------|------|--------------|---------|
| |ghost in the system| |[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)|
| |reverse engineer| |[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
| |taking off|static analysis|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
| |mr game and watch|java, sha-1, sha-256, md5|[AUCTF 2020](https://gitlab.com/b0uldrr/auctf-2020)|
| |sora|static analysis|[AUCTF 2020](https://gitlab.com/b0uldrr/auctf-2020)|
|✔|put your thang down flip it and reverse it|static analysis, live analysis|[DawgCTF 2020](https://gitlab.com/b0uldrr/dawgctf-2020)|
| |dangerous live and malicious code|javascript|[WPICTF 2020](https://gitlab.com/b0uldrr/wpictf-2020)|
|✔|NotWannasigh|c, rand, srand, xor, static analysis|[WPICTF 2020](https://gitlab.com/b0uldrr/wpictf-2020)
| |angrmanagement|angr, static analysis|[Tamu CTF 2020](https://gitlab.com/b0uldrr/tamuctf-2020)|
| |ez |python |[Houseplant CTF 2020](https://gitlab.com/b0uldrr/houseplant-ctf-2020)|
| |pz |python |[Houseplant CTF 2020](https://gitlab.com/b0uldrr/houseplant-ctf-2020)|
| |lemon |python |[Houseplant CTF 2020](https://gitlab.com/b0uldrr/houseplant-ctf-2020)|
| |squeezy |python, XOR, base64 |[Houseplant CTF 2020](https://gitlab.com/b0uldrr/houseplant-ctf-2020)|
| |thedanzman |python, XOR, base64, rot13 |[Houseplant CTF 2020](https://gitlab.com/b0uldrr/houseplant-ctf-2020)|
| |bendy |java |[Houseplant CTF 2020](https://gitlab.com/b0uldrr/houseplant-ctf-2020)|
| |breakable |java |[Houseplant CTF 2020](https://gitlab.com/b0uldrr/houseplant-ctf-2020)|
| |ice cream bytes|java|[HSCTF 2020](https://gitlab.com/b0uldrr/hsctf-2020/)|
| |recursion reverse|java, brute force|[HSCTF 2020](https://gitlab.com/b0uldrr/hsctf-2020/)|
| |formatting|static analysis, live analysis|[DownUnder CTF 2020](https://gitlab.com/b0uldrr/downunder-ctf-2020)|
| |change|javascript|[Syskron CTF 2020](https://gitlab.com/b0uldrr/syskronctf-2020)|
| |keygen|static analysis, ghidra|[Syskron CTF 2020](https://gitlab.com/b0uldrr/syskronctf-2020)|
| |generic flag checker 1|static analysis|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
| |generic flag checker 2|dynamic analysis|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
|✔|patches|binary patching, ghidra, gdb|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
| |that's not crypto|python, bruteforce|[justCTF 2020](https://gitlab.com/b0uldrr/justctf-2020])|
| |not really math|python|[HSCTF 2021](https://gitlab.com/b0uldrr/hsctf-2021)|
| |seeded randomizer|java, random|[HSCTF 2021](https://gitlab.com/b0uldrr/hsctf-2021)|
| |warmup rev|java|[HSCTF 2021](https://gitlab.com/b0uldrr/hsctf-2021)|

### Web
| Like | Name | Key Concepts | Writeup |
|------|------|--------------|---------|
| |robots yeah I know pretty obvious|robots.txt|[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)|
| |no sleep| |[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)| 
| |uwu?|Burp|[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)| 
| |what's in the box|Javascript|[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)| 
| |web invaders|OSINT|[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)| 
| |SQL breaker|SQL injection|[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
| |cookie moster|cookies|[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
| |browser bias|user-agent|[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
| |consolation|javascript, browser console|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
|✔|secret agents|user-agent, SQL injection, burp|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
|✔|xmas still stands|stored XSS, webhook, cookies|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
|✔|broken tokens|jwt|[HSCTF 2020](https://gitlab.com/b0uldrr/hsctf-2020/)|
| |debt simulator|javascript, post, curl|[HSCTF 2020](https://gitlab.com/b0uldrr/hsctf-2020/)|
| |very safe login|simple, source code|[HSCTF 2020](https://gitlab.com/b0uldrr/hsctf-2020/)|
| |calculator|php, exec, shell|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
| |cookie recipe|cookies|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
| |login|sql injection|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
|✔|meet the team|git|[NahamCon CTF 2021](https://gitlab.com/b0uldrr/nahamcon-ctf-2021)|
| |secure|javascript, sql injection, burp suite|[Redpwn CTF 2021](https://gitlab.com/b0uldrr/redpwn-ctf-2021)|

### Forensics
| Like | Name | Key Concepts | Writeup |
|------|------|--------------|---------|
| |BTS-crazed| |[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)| 
| |alergic college application|Text encoding|[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)| 
| |BASmatic ricE 64| |[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)| 
| |bash history|base64|[Syskron CTF 2020](https://gitlab.com/b0uldrr/syskronctf-2020)|
| |pollex|steg, binwalk, jpg|[NahamCon CTF 2021](https://gitlab.com/b0uldrr/nahamcon-ctf-2021)|

### Crypto
| Like | Name | Key Concepts | Writeup |
|------|------|--------------|---------|
| |don't give the GIANt a COOKie|cookies|[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)| 
| |wrong way| |[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)| 
| |pigs fly| |[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
| |stop the bot | |[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
| |follow me|burp|[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
| |baby RSA| |[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
|✔|random ECB|ECB, block cipher, known plaintext|[UTCTF 2020](https://gitlab.com/b0uldrr/utctf-2020)|
| |illuminati confirmed|RSA, low exponent value, Chinese Remainder Theorem|[WPICTF 2020](https://gitlab.com/b0uldrr/wpictf-2020)|
| |error 0|prog|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
| |car keys|keyed ceasar cipher|[NahamCon CTF 2021](https://gitlab.com/b0uldrr/nahamcon-ctf-2021)|
| |esab64|base64, rev|[NahamCon CTF 2021](https://gitlab.com/b0uldrr/nahamcon-ctf-2021)|

### Misc
| Like | Name | Key Concepts | Writeup |
|------|------|--------------|---------|
| |work in progress| |[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)|
| |snakes| |[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)|
| |password crack|prog|[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
| |robot talk|prog|[Neverlan CTF 2020](https://gitlab.com/b0uldrr/neverlan-ctf-2020)|
| |inputter|prog|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
| |shifter|prog|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
| |miracle mile|prog|[DawgCTF 2020](https://gitlab.com/b0uldrr/dawgctf-2020)|
| |addition|python|[DownUnder CTF 2020](https://gitlab.com/b0uldrr/downunder-ctf-2020)|
| |koala habitat|audio, morse code|[DownUnder CTF 2020](https://gitlab.com/b0uldrr/downunder-ctf-2020)|
| |pretty good pitfall|pgp, gpg|[DownUnder CTF 2020](https://gitlab.com/b0uldrr/downunder-ctf-2020)|
| |security|pgp, gpg|[Syskron CTF 2020](https://gitlab.com/b0uldrr/syskronctf-2020)|
| |dr j's vegetable factory #3|prog, buble sort|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
| |world trip|prog, geopy, coordinates|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
| |iot itchy|mqtt, iot|[NahamCon CTF 2021](https://gitlab.com/b0uldrr/nahamcon-ctf-2021)|
|✔|iot itchy & scratchy|mqtt, iot, base64|[NahamCon CTF 2021](https://gitlab.com/b0uldrr/nahamcon-ctf-2021)|
| |not really math| |[HSCTF 2021](https://gitlab.com/b0uldrr/hsctf-2021)|



### OSINT
| Like | Name | Key Concepts | Writeup |
|------|------|--------------|---------|
| |gus| |spiderfoot, github|[NahamCon CTF 2021](https://gitlab.com/b0uldrr/nahamcon-ctf-2021)|


### Image & Audio Manipulation
| Like | Name | Key Concepts | Writeup |
|------|------|--------------|---------|
|✔|screams|python, image, audio, steg|[RiceTeaCatPanda CTF 2020](https://gitlab.com/b0uldrr/riceteacatpanda-ctf-2020)|
| |wacko images|python, image, steg|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
| |msd|python, image, steg|[Angstrom CTF 2020](https://gitlab.com/b0uldrr/angstrom-2020)|
| |satan's jigsaw |python, image |[Houseplant CTF 2020](https://gitlab.com/b0uldrr/houseplant-ctf-2020)|

---

### Unsolved - Revsit for Practice
| Name | Key Concepts | CTF |
|------|--------------|-----|
|pwnagotchi|binary pwn, bof, rop|[HSCTF 2020](https://gitlab.com/b0uldrr/hsctf-2020/)|
|return to what|ret2libc, bof|[DownUnder CTF 2020](https://gitlab.com/b0uldrr/downunder-ctf-2020)|
|baby rsa|rsa|[DownUnder CTF 2020](https://gitlab.com/b0uldrr/downunder-ctf-2020)|
|added protection|rev|[DownUnder CTF 2020](https://gitlab.com/b0uldrr/downunder-ctf-2020)|
|format|printf, format string|[Newark Academy CTF 2020](https://gitlab.com/b0uldrr/newark-academy-ctf-2020)|
